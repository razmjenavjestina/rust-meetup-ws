use actix_web::{App, HttpRequest, Responder, server};

fn index_get(_req: &HttpRequest) -> impl Responder {
    "Hello, HTTP GET world!\n"
}

fn index_post(_req: &HttpRequest) -> impl Responder {
    "Hello, HTTP POST world!\n"
}

fn main() {
    server::new(|| {
        App::new()
            .resource("/", |r| {
                r.get().f(index_get);
                r.post().f(index_post);
            })
    })
    .bind("127.0.0.1:8000")
    .expect("bind() unsuccessful")
    .run();
}
