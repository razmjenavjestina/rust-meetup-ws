use actix_web::{server, http::Method, App, HttpRequest, Responder};

fn index(_req: HttpRequest) -> impl Responder {
    "hello!"
}

fn main() {
    server::new(
        || App::new()
            .route("/index", Method::GET, index)
            .finish()
    ).bind("127.0.0.1:8000").unwrap().run();
}
