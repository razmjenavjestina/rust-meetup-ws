use actix::prelude::*;
use actix_web::{App, server, HttpRequest, AsyncResponder, FutureResponse, HttpResponse};
use futures::future::Future;

// --- Counter is our struct which has internal state and is able to receive messages --------------

struct Counter {
    cnt: usize,
}

impl Actor for Counter {
    type Context = SyncContext<Self>;
}

// --- Incr is a very primitive message for the Counter actor to react to --------------------------

struct Incr {}

impl Message for Incr {
    type Result = Result<usize, ()>;
}

impl Handler<Incr> for Counter {
    type Result = Result<usize, ()>;

    fn handle(&mut self, _msg: Incr, _ : &mut Self::Context) -> Self::Result {
        self.cnt += 1;
        println!("cnt: {}", self.cnt);

        Ok(self.cnt)
    }
}

// --- our application state only holds the Counter actor address ----------------------------------

struct AppState {
    counter: Addr<Counter>,
}

// when dealing with actors, we're actually dealing with futures

fn handler(req: HttpRequest<AppState>) -> FutureResponse<HttpResponse> {
    req.state()
       .counter
       // we send the Incr message to our actor and kindly ask it to increment
       .send(Incr {})
       .from_err()
       .and_then(move |res| match res {
          Ok(_)  => Ok(
              HttpResponse::Ok().body(format!("Counter state: {:?}", res))
          ),
          Err(_) => Ok(HttpResponse::InternalServerError().body("")),
       })
       .responder()
}

fn main() {
    let _sys = actix::System::new("Rust Meetup");
    let address: Addr<Counter> = SyncArbiter::start(1, move || Counter { cnt: 0 });

    server::new(move || {
        App::with_state(AppState { counter: address.clone() })
            .resource("/", |r| { r.get().with(handler); })
    })
    .bind("127.0.0.1:8000")
    .expect("bind() unsuccessful")
    .run();
}
