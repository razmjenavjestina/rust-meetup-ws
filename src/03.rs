use actix_web::{App, HttpRequest, Responder, server};
use std::sync::{Arc, Mutex};

struct AppState {
    counter: Arc<Mutex<usize>>,
}

fn handler(req: &HttpRequest<AppState>) -> impl Responder {
    let mut cnt = req.state().counter.lock().unwrap();
    *cnt += 1;

    format!("Request number: {}\n", cnt)
}

fn main() {
    let counter = Arc::new(Mutex::new(0));

    server::new(move || {
        App::with_state(AppState { counter: counter.clone() })
            .resource("/", |r| { r.get().f(handler); })
    })
    .bind("127.0.0.1:8000")
    .expect("bind() unsuccessful")
    .run();
}
