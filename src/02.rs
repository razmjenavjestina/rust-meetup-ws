extern crate actix_web;

use actix_web::{App, Json, Result, server};
use serde::{Deserialize};

#[derive(Deserialize)]
struct AppRequest {
    parameter: String,
}

fn handler(app_req: Json<AppRequest>) -> Result<String> {
    Ok(format!("Parameter: {}\n", app_req.parameter))
}

fn main() {
    server::new(|| {
        App::new()
            .resource("/", |r| { r.post().with(handler); })
    })
    .bind("127.0.0.1:8000")
    .expect("bind() unsuccessful")
    .run();
}
