use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct Answer {
    answer_text: String,
    correct: bool,
}

#[derive(Serialize, Deserialize, Debug)]
struct Question {
    question_text: String,
    answers: Vec<Answer>,
}

fn main() {
    let q = Question {
        question_text: "What is the square root of 2?".to_string(),
        answers: vec![
            Answer {
                answer_text: "approx. 1.41".to_string(),
                correct: true,
            },
            Answer {
                answer_text: "approx. 0.71".to_string(),
                correct: false,
            },
        ],
    };

    let q_json = serde_json::to_string(&q).unwrap();
    println!("{}", q_json);
}
