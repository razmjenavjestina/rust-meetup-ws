#+STARTUP: content indent

* Rust u industriji

  "Rust is a major advancement in industrial programming languages due in large
  part to its success in bridging the gap between low-level systems programming
  and high-level application programming. This success has ultimately empowered
  programmers to more easily build reliable and efficient software, and at its
  heart lies anovel approach to ownership that balances type system
  expressivity with usability."
  
  ~ *Oxide: The Essence of Rust*
    Aaron Weiss, Daniel Patterson, Nicholas D. Matsakis, Amal Ahmed
    ([[https://arxiv.org/pdf/1903.00982.pdf][arXiv:1903.00982]])

* Web servisi u high-performance softveru

  - *TVbeat, pisanje web servisa i C++*

    - pisanje web servisa u C++-u nije sreća

    - rješenje: Lua / OpenResty

    - hrvanje s neusklađenošću jezika, libraryja, serijalizacijskih mehanizama...

  - *Rust kao rješenje*

    - jezik koji podržava i high-level i low-level građevne jedinice

* Rust i web development

** [[https://arewewebyet.org][arewewebyet.org]]

- pregled web ekosustava po kategorijama

** HTTP klijenti

- [[https://crates.io/crates/curl][curl]] (libcurl wrapper)

- [[https://hyper.rs/guides/client/basic/][hyper]]

- [[https://github.com/seanmonstar/reqwest][reqwest]]

** HTTP serveri

- [[https://rocket.rs/][Rocket]] (Hyper -> Tokio)

- [[http://ironframework.io/][Iron]] (Hyper -> Tokio)

- [[https://actix.rs/][actix + actix-web]] (h2 + Tokio)

** usporedbe s Djangom / Railsima / Symfonyjem

- ništa od ovoga nije Django

- uz nešto ljepila, može postati nešto blisko nekom od popularnijih high-level frameworkova

- približno analogno: Flask, Express.js, Compojure, ...

* Rocket

- vjerojatno najpotpuniji framework po količini featurea

- Rust nightly-only

#+BEGIN_SRC rust
#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

#[get("/hello/<name>/<age>")]
fn hello(name: String, age: u8) -> String {
  format!("Hello, {} year old named {}!", age, name)
}

fn main() {
  rocket::ignite().mount("/", routes![hello]).launch();
} 
#+END_SRC

* actix / actix-web

** actix - actori za Rust

- zrela implementacija actora u Rustu nad Tokio async frameworkom

- bilo koji Rust tip može implementirati =Actor= trait i reagirati na poruke

- "arbiter" je event loop koji kontrolira actore

- arbitri i actori mogu biti i sinkroni

** actix-web - web framework nad actixom

- bogat API za razvoj strongly-typed HTTP aplikacija

- *routing*: dispatch po parametrima requesta
  (metoda, URL, content type, scope)

- *extractors*: izvlačenje podataka iz requesta u Rust strukture
  (path, body, query string)

- podržava HTTP2 i TLS

#+BEGIN_SRC rust :exports code :tangle src/00.rs
use actix_web::{server, http::Method, App, HttpRequest, Responder};

fn index(_req: HttpRequest) -> impl Responder {
    "hello!"
}

fn main() {
    server::new(
        || App::new()
            .route("/index", Method::GET, index)
            .finish()
    ).bind("127.0.0.1:8000").unwrap().run();
}
#+END_SRC

*** nekoliko jednostavnijih primjera

**** trivijalni POST / GET handler

#+BEGIN_SRC rust :exports code :tangle src/01.rs
use actix_web::{App, HttpRequest, Responder, server};

fn index_get(_req: &HttpRequest) -> impl Responder {
    "Hello, HTTP GET world!\n"
}

fn index_post(_req: &HttpRequest) -> impl Responder {
    "Hello, HTTP POST world!\n"
}

fn main() {
    server::new(|| {
        App::new()
            .resource("/", |r| {
                r.get().f(index_get);
                r.post().f(index_post);
            })
    })
    .bind("127.0.0.1:8000")
    .expect("bind() unsuccessful")
    .run();
}
#+END_SRC

**** Intermezzo: =serde=

- serijalizacijski framework za Rust

- out-of-the-box serijalizacija većine built-in tipova i izvedenih struktura kroz =#[derive]= mehanizam

- podržava JSON, CBOR, TOML, YAML, Pickle, Avro, ...

#+BEGIN_SRC rust :exports code :tangle src/serde_ex.rs
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
struct Answer {
    answer_text: String,
    correct: bool,
}

#[derive(Serialize, Deserialize, Debug)]
struct Question {
    question_text: String,
    answers: Vec<Answer>,
}

fn main() {
    let q = Question {
        question_text: "What is the square root of 2?".to_string(),
        answers: vec![
            Answer {
                answer_text: "approx. 1.41".to_string(),
                correct: true,
            },
            Answer {
                answer_text: "approx. 0.71".to_string(),
                correct: false,
            },
        ],
    };

    let q_json = serde_json::to_string(&q).unwrap();
    println!("{}", q_json);
}
#+END_SRC

**** deserijalizacija u JSON

#+BEGIN_SRC rust :exports code :tangle src/02.rs
extern crate actix_web;

use actix_web::{App, Json, Result, server};
use serde::{Deserialize};

#[derive(Deserialize)]
struct AppRequest {
    parameter: String,
}

fn handler(app_req: Json<AppRequest>) -> Result<String> {
    Ok(format!("Parameter: {}\n", app_req.parameter))
}

fn main() {
    server::new(|| {
        App::new()
            .resource("/", |r| { r.post().with(handler); })
    })
    .bind("127.0.0.1:8000")
    .expect("bind() unsuccessful")
    .run();
}
#+END_SRC

#+BEGIN_SRC sh
curl -d '{ "parameter": "Hello" }' -X POST -H 'Content-Type: application/json' http://127.0.0.1:8000/
#+END_SRC

*** app state

- actix-webov server je multithreadan => thread-safe strukture u stateu!

#+BEGIN_SRC rust :exports code :tangle src/03.rs
use actix_web::{App, HttpRequest, Responder, server};
use std::sync::{Arc, Mutex};

struct AppState {
    counter: Arc<Mutex<usize>>,
}

fn handler(req: &HttpRequest<AppState>) -> impl Responder {
    let mut cnt = req.state().counter.lock().unwrap();
    *cnt += 1;

    format!("Request number: {}\n", cnt)
}

fn main() {
    let counter = Arc::new(Mutex::new(0));

    server::new(move || {
        App::with_state(AppState { counter: counter.clone() })
            .resource("/", |r| { r.get().f(handler); })
    })
    .bind("127.0.0.1:8000")
    .expect("bind() unsuccessful")
    .run();
}
#+END_SRC

*** primjer korištenja actora za stanje aplikacije

- [[https://github.com/actix/examples/tree/master/simple-auth-server][=simple-auth-server=]] primjer u actixovom =examples/= folderu

- koristi database pool kao actor, a queryji su poruke

#+BEGIN_SRC rust :exports code :tangle src/04.rs
use actix::prelude::*;
use actix_web::{App, server, HttpRequest, AsyncResponder, FutureResponse, HttpResponse};
use futures::future::Future;

// --- Counter is our struct which has internal state and is able to receive messages --------------

struct Counter {
    cnt: usize,
}

impl Actor for Counter {
    type Context = SyncContext<Self>;
}

// --- Incr is a very primitive message for the Counter actor to react to --------------------------

struct Incr {}

impl Message for Incr {
    type Result = Result<usize, ()>;
}

impl Handler<Incr> for Counter {
    type Result = Result<usize, ()>;

    fn handle(&mut self, _msg: Incr, _ : &mut Self::Context) -> Self::Result {
        self.cnt += 1;
        println!("cnt: {}", self.cnt);

        Ok(self.cnt)
    }
}

// --- our application state only holds the Counter actor address ----------------------------------

struct AppState {
    counter: Addr<Counter>,
}

// when dealing with actors, we're actually dealing with futures

fn handler(req: HttpRequest<AppState>) -> FutureResponse<HttpResponse> {
    req.state()
       .counter
       // we send the Incr message to our actor and kindly ask it to increment
       .send(Incr {})
       .from_err()
       .and_then(move |res| match res {
          Ok(_)  => Ok(
              HttpResponse::Ok().body(format!("Counter state: {:?}", res))
          ),
          Err(_) => Ok(HttpResponse::InternalServerError().body("")),
       })
       .responder()
}

fn main() {
    let _sys = actix::System::new("Rust Meetup");
    let address: Addr<Counter> = SyncArbiter::start(1, move || Counter { cnt: 0 });

    server::new(move || {
        App::with_state(AppState { counter: address.clone() })
            .resource("/", |r| { r.get().with(handler); })
    })
    .bind("127.0.0.1:8000")
    .expect("bind() unsuccessful")
    .run();
}
#+END_SRC

* Zašto uopće Rust?

** ekosustav

- type-safety kao primarni feature

- fokus na asinkronost

- solidna podrška za "standardne" web stvarčice
  (relacijske baze, k/v baze, 0MQ, ...)

** tipski sustav

- *=Result<T, E>= i =Option<T>=*

  - standardizirani i prihvaćeni načini za error handling

  - bogat API oko ovih tipova

    =and_then= (flatmap)
    =map_err= (transformacije među errorima)
    ...

- *algebarski tipovi*

- *traitovi kao mehanizam za generičko programiranje*

  - serijalizacija / deserijalizacija

  - actix: =impl Responder=

  - error types: =impl Error=

#+BEGIN_SRC rust
#[derive(Debug)]
enum WebError {
    NotAllowed,
    InternalServerError,
    IAmATeapot,
}

impl Error for WebError {}

impl Display for WebError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            WebError::NotAllowed          => write!(f, "Method Not Allowed"),
            WebError::InternalServerError => write!(f, "Internal Server Error"),
            WebError::IAmATeapot          => write!(f, "I'm a Teapot"),
        }
  }
}

fn do_thingy() -> Result<String, WebError> {
    Err(WebError::NotAllowed)
}

// assuming:
type HttpError = u16;

fn do_thingy_handler() -> Result<HttpResponse, HttpError> {
    do_thingy().map_err(|err| {
        match err {
            WebError::NotAllowed          => 405,
            WebError::IAmATeapot          => 418,
            WebError::InternalServerError => 500,
        }
    })
}
#+END_SRC

** sustav makroa

- nije Lisp / Clojure, ali jako ugodan i pristojno moćan mehanizam kontrole nad sintaksom

- [[https://github.com/bodil/typed-html][typed-html]]: JSX za Rust

#+BEGIN_SRC rust
let mut doc: DOMTree<String> = html!(
    <div><p>Rust Meetup 201903</p></div>
);
#+END_SRC

- zgodno za pojednostavljivanje dijelova koda

#+BEGIN_SRC rust
App::new()
    .resource("/", |r| {
        r.get().with(handler_get);
        r.post().with(handler_post);
    })

// <=> //

app!(
    "/" => [
        "GET"  => with(handler_get),
        "POST" => with(handler_post),
    ]
)
#+END_SRC

** interop

- postojeći Rust kod je udaljen jednu =use= direktivu

- interop s C/C++ libraryjima kroz jako robusnu FFI infrastrukturu

** WebAssembly

- Rust ima izvrsnu podršku za WASM kao target

- """izomorfni""" Rust

* Fin.

Kod & bilješke:

https://gitlab.com/razmjenavjestina/rust-meetup-ws

** =cargo.toml=

#+BEGIN_SRC toml :exports code :tangle Cargo.toml
[package]
name = "rust-ws"
version = "0.1.0"
authors = ["Nikola Plejic <nikola@plejic.com>"]
edition = "2018"

[[bin]]
name = "00"
path = "src/00.rs"

[[bin]]
name = "01"
path = "src/01.rs"

[[bin]]
name = "serde_ex"
path = "src/serde_ex.rs"

[[bin]]
name = "02"
path = "src/02.rs"

[[bin]]
name = "03"
path = "src/03.rs"

[[bin]]
name = "04"
path = "src/04.rs"

[dependencies]
actix = "0.7"
actix-web = "0.7"

serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
futures = "0.1"
#+END_SRC
